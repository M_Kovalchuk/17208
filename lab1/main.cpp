#include <iostream>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <assert.h>
#include "CircularBuffer.h"

TEST( ctor , cb){
    CircularBuffer cb(10);
    ASSERT_EQ(cb.Capacity() , 10);
    const char t = 'A';
    CircularBuffer cd2(10 , t );
    for(int i = 0 ; i < 10 ; i++){
        ASSERT_EQ(cd2[i] , 'A');
    }
}

TEST( opAcess , cb){
    CircularBuffer cb(10 , 'S');
    for(int i = 0 ; i < 10 ; i++){
        ASSERT_EQ( cb[i] , 'S');
    }
}

TEST( insert , cb ){
    CircularBuffer cb(16);
    cb.insert( 2 , 'A');
    ASSERT_EQ(cb[2] , 'A');
    ASSERT_ANY_THROW(cb.insert( -2 , 'A'));
    ASSERT_ANY_THROW(cb.insert( 8000 , 'A'));
    ASSERT_EQ( cb.begin() , 2);
    ASSERT_EQ( cb.Size() , 1);
    cb.insert( 8 , 'A');
    ASSERT_EQ( cb.Size() , 2);
    cb.insert( 8 , 'B');
    ASSERT_EQ(cb[8] , 'B');
}

TEST( front , cb){
    CircularBuffer cb(10);
    ASSERT_ANY_THROW(cb.front());
    cb.insert( 2 , 'A');
    ASSERT_EQ( cb.front() , 'A');
    cb.insert( 1 , 'B');
    ASSERT_EQ( cb.front() , 'A');
}

TEST( back , cb){
    CircularBuffer cb(10);
    ASSERT_ANY_THROW( cb.back());
    cb.insert( 2 , 'A');
    ASSERT_EQ( cb.back() , cb.front());
    cb.insert( 1 , 'B');
    ASSERT_EQ( cb.back() , 'B');
}

TEST( erase , cb){
    CircularBuffer cb( 64 , 'A');
    cb.rotate(30);
    cb.erase( 30 , 40);
    ASSERT_ANY_THROW(cb.erase( -2 , 3));
    ASSERT_ANY_THROW(cb.erase( 40 , 20));
    ASSERT_ANY_THROW(cb.erase( 2 , 10000));
    for(int i = 0 ; i < 64 ; i++){
        if( i >= 30 && i <= 40)
            ASSERT_EQ( cb[i] , '\0');
        else
            ASSERT_EQ( cb[i] , 'A');
    }
    ASSERT_EQ( cb.begin() , 41);
    cb.erase( 30 , 64);
    ASSERT_EQ( cb.begin() , 0);
}

TEST (is_linearized , cb){
    CircularBuffer cb(10);
    ASSERT_TRUE( cb.is_linearized());
    cb.insert( 2 , 'A');
    ASSERT_FALSE( cb.is_linearized());
    for(int i = 0 ; i < 10 ; i++){
        cb.insert( i , 'B');
    }
    ASSERT_FALSE( cb.is_linearized());
    cb.rotate(0);
    ASSERT_TRUE( cb.is_linearized());
    cb.erase(5 , 7);
    ASSERT_FALSE( cb.is_linearized());
}

TEST(liearize , cb){
    CircularBuffer *cb = new CircularBuffer(10);
    cb->rotate(8);
    cb->linearize();
    ASSERT_EQ(cb->begin() , 0);
    cb->insert( 5 , 'A');
    cb->linearize();
    ASSERT_EQ( cb->begin() , 0);
    ASSERT_EQ( cb->Size() , 1);
    for(int i = 0 ; i < 10 ; i++){
        cb->insert( i , 'B');
    }
    cb->rotate(9);
    cb->linearize();
    ASSERT_EQ( cb->begin() , 0);
    ASSERT_EQ( cb->Size() , 10);
    ASSERT_TRUE( cb->is_linearized());
    cb->rotate(6);
    cb->erase(5 , 8);
    cb->linearize();
    ASSERT_EQ( cb->begin() , 0);
    ASSERT_EQ( cb->Size() , 6);
    ASSERT_TRUE( cb->is_linearized());
    delete cb;
}

TEST( at , cb){
    CircularBuffer cb(10);
    ASSERT_EQ( cb.at(2) , '\0');
    cb.insert(2 , 'A');
    ASSERT_EQ( cb.at(2) , 'A');
    ASSERT_ANY_THROW( cb.at(11));
    ASSERT_ANY_THROW( cb.at(-1));

}

TEST (set_capacity , cb){
    CircularBuffer *cb = new CircularBuffer(10 , 'A');
    cb->set_capacity(16);
    ASSERT_EQ(cb->Capacity() , 16);
    ASSERT_EQ(cb->begin() , 0);
    ASSERT_EQ(cb->Size() , 10);
    ASSERT_EQ(cb->reserve() , 6);
    for(int i = 10 ; i < 16 ; i++){
        cb->insert( i , 'A');
    }
    cb->set_capacity(5);
    ASSERT_EQ(cb->Capacity() , 5);
    ASSERT_EQ(cb->begin() , 0);
    ASSERT_EQ(cb->Size() , 5);
    ASSERT_EQ(cb->reserve() , 0);
    delete cb;
}

TEST( resize , cb){
    CircularBuffer *cb = new CircularBuffer(10 , 'A');
    cb->resize( 16 , 'B');
    ASSERT_TRUE(cb->is_linearized());
    ASSERT_EQ(cb->begin() , 0);
    ASSERT_EQ(cb->Size() , 16);
    for(int i = 0 ; i < 16 ; i++){
        if( i < 10)
            ASSERT_EQ( cb->at(i) , 'A');
        else
            ASSERT_EQ( cb->at(i) , 'B');
    }
    cb->resize( 5 , 'C');
    ASSERT_TRUE(cb->is_linearized());
    ASSERT_EQ(cb->begin() , 0);
    ASSERT_EQ(cb->Size() , 5);
    for(int i = 0 ; i < 5 ; i++){
        if( i < 10)
            ASSERT_EQ( cb->at(i) , 'A');
    }
    delete cb;
}

TEST( opEQ , cb){
    CircularBuffer cb1(10 , 'A');
    CircularBuffer cb2(10 , 'A');
    ASSERT_TRUE( cb1 == cb2 );
    cb2.set_capacity(16);
    ASSERT_TRUE( cb1 == cb2 );
    cb1.set_capacity( 32);
    cb1.rotate(7);
    cb1.erase(1 , 3);
    for(int i = 21 ; i < 24 ; i++)
        cb1.insert( i , 'A');
    ASSERT_TRUE( cb1 == cb2);
}

TEST( opNotEQ , cb){
    CircularBuffer cb1(10 , 'A');
    CircularBuffer cb2(10 , 'B');
    ASSERT_TRUE( cb1 != cb2);
}

TEST( clear , cb){
    CircularBuffer cb( 16 , 'A');
    cb.clear();
    ASSERT_TRUE(cb.empty());
}

//TEST( swap , cb){
//    CircularBuffer *cb1 = new CircularBuffer(10 , 'A');
//    CircularBuffer *cb2 = new CircularBuffer(10 , 'B');
//    cb1->swap(cb2);
//    ASSERT_EQ( cb1->at(2) , 'B');
//    ASSERT_EQ( cb2->at(5) , 'A');
//    delete cb1;
//    delete cb2;
//}

TEST( push_back , cb){
    CircularBuffer cb(50);
    for(int i = 0 ; i < 101 ; i++){
        cb.push_back('Q');
    }
    ASSERT_EQ( cb.begin() , 1);
    for(int i = 0 ; i < 50 ; i++){
        ASSERT_EQ( cb[i] , 'Q');
    }
}

TEST( push_front , cb){
    CircularBuffer cb(10 , 'A');
    cb.push_front('B');
    ASSERT_EQ( cb.begin() , 9);
    ASSERT_EQ( cb[cb.begin()] , 'B');
    cb.rotate(9);
    cb.push_front('C');
    ASSERT_EQ( cb.begin() , 8);
    ASSERT_EQ( cb[cb.begin()] , 'C');
}

TEST( pop_back , cb){
    CircularBuffer cb(10 , 'A');
    cb.pop_back();
    ASSERT_EQ( cb.Size() , 9);
    ASSERT_EQ( cb[9] , '\0');
    cb.pop_back();
    cb.rotate(5);
    cb.pop_back();
    ASSERT_EQ( cb.Size() , 7);
    ASSERT_EQ( cb[4] , '\0');
}

TEST( pop_front , cb){
    CircularBuffer cb( 10 , 'A');
    cb.pop_front();
    ASSERT_EQ( cb.begin() , 1);
    ASSERT_EQ( cb[0] , '\0');
    cb.rotate(9);
    cb.pop_front();
    ASSERT_EQ( cb.begin() , 0);
    ASSERT_EQ( cb[9] , '\0');
}

int main(int argc , char *argv[]){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
