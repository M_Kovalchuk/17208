#ifndef CIRCULAR_BUFFER
#define CIRCULAR_BUFFER

#include <algorithm>

typedef char value_type;

const int defaultCapcity = 32;

struct NODE{
    value_type data = 0;
    bool is_empty = true;
};

class CircularBuffer{
public:
    CircularBuffer(): capacity(defaultCapcity) , head(0){
        arr = new NODE [capacity];
    };

    ~CircularBuffer(){
        delete [] arr;
    };

    CircularBuffer(const CircularBuffer & cb): head(0), size(cb.size) , capacity(cb.capacity) , arr(cb.arr){};

    //Конструирует буфер заданной ёмкости.

    explicit CircularBuffer(int newCapacity): head(0), capacity(newCapacity){
        arr = new NODE [capacity];
    };

//Конструирует буфер заданной ёмкости, целиком заполняет его элементом elem.

    CircularBuffer(int newCapacity, const value_type & elem): head(0) , capacity(newCapacity) , size(newCapacity) {
        arr = new NODE [capacity];
        for(int i = 0 ; i < capacity ; i++ ){
            arr[i].data = elem;
            arr[i].is_empty = false;
        }
    };

//Доступ по индексу. Не проверяют правильность индекса.

    value_type & operator[](int i){                                    //тест есть
        return(arr[i].data);
    };

//    const value_type & operator[](int i) const;

//Доступ по индексу. Методы бросают исключение в случае неверного индекса.

    value_type & at(int i){                                                  //тест есть
        if(i > capacity || i < 0){
            throw 1;
        }
        return(arr[i].data);
    };

//    const value_type & at(int i) const;

    value_type & front(){                                                    //тест есть
        if(empty()){
            throw -1;
        }
        return arr[head].data;
    }; //Ссылка на первый элемент.

    value_type & back(){                                                     //тест есть
        if(empty()){
            throw -1;
        }
        if(size == 1){
            return front();
        }
        int sizeCnt = 0;
        for(int i = 0 ; i < capacity ; i++ ){
            if(!arr[(head + i) % capacity].is_empty) {
                if (sizeCnt + 1 == size)
                    return arr[(head + i) % capacity].data;
                else
                    sizeCnt++;
            }
        }
        throw -2;
    }; //Ссылка на последний элемент.

//Линеаризация - сдвинуть кольцевой буфер так, что его первый элемент
//переместится в начало аллоцированной памяти. Возвращает указатель
//на первый элемент.
    NODE *linearize(){                                                 //тест есть
        if(is_linearized()){
            if(empty())
                rotate(0);
            return arr;
        }
        NODE *newArr = new NODE [capacity];
        int newSize = 0;
        for(int i = 0 ; i < capacity && newSize < size ; i++){
            if(!arr[(head + i) % capacity].is_empty){
                newArr[newSize].data = arr[(head + i) % capacity].data;
                newArr[newSize].is_empty = false;
                newSize++;
            }
        }
        delete [] arr;
        arr = newArr;
        rotate(0);
        return arr;
    };

//Проверяет, является ли буфер линеаризованным.

    bool is_linearized() const{                                                 //тест есть
        if(empty())
            return true;
        if(head != 0)
            return false;
        int s = 0;
        for(int i = 0 ; i < capacity ; i++){
            if(arr[i].is_empty && i < size) {
                return false;
            }
        }
        return true;
    };

//Сдвигает буфер так, что по нулевому индексу окажется элемент

//с индексом new_begin.

    void rotate(int new_begin){
        head = new_begin;
    };

//Количество элементов, хранящихся в буфере.

    int Size() const{
        return size;
    };

    bool empty() const{
        return size == 0;
    };

//true, если size() == capacity().

    bool full() const{
        return size == capacity;
    };

//Количество свободных ячеек в буфере.

    int reserve() const{
        return capacity - size;
    };

    int begin() const{
        return head;
    }

//ёмкость буфера

    int Capacity() const{
        return capacity;
    };

    void set_capacity(int new_capacity){                                      //тест есть
        NODE *newArr = new NODE [new_capacity];
        linearize();
        int newSize = 0;
        for(int i = 0 ; i < capacity && i < new_capacity && newSize < size ; i++){
            newArr[i].data = arr[i].data;
            newArr[i].is_empty = false;
            newSize++;
        }
        size = newSize;
        delete [] arr;
        arr = newArr;
        capacity = new_capacity;
    };

//Изменяет размер буфера.

//В случае расширения, новые элементы заполняются элементом item.

    void resize(int new_capacity, const value_type & item = value_type()){                    //тест есть
        NODE *newArr = new NODE [new_capacity];
        linearize();
        for(int i = 0 ; i < new_capacity ; i++){
            newArr[i].is_empty = false;
            if( i < size ){
                newArr[i].data = arr[i].data;
            }
            else{
                newArr[i].data = item;
            }
        }
        delete [] arr;
        arr = newArr;
        capacity = new_capacity;
        size = new_capacity;
    };

    int index(int i){
        return ((head + i)%capacity);
    }

    bool operator==(const CircularBuffer &b ){                      //тест есть
        if(size != b.size) {
            return false;
        }
        int cnt = 0;
        int eq = 0;
        bool found = false;
        for(int i = 0 ; i < capacity ; i++){
            if(!arr[((head + i)%capacity)].is_empty) {
                while( cnt < b.capacity && !found){
                    if (!b.arr[(b.head + cnt) % b.capacity].is_empty) {
                        if (arr[(head + i) % capacity].data != b.arr[(b.head + cnt) % b.capacity].data)
                            return false;
                        cnt++;
                        eq++;
                        found = true;
                    }
                    else
                        cnt++;
                }
                found = false;
                if(eq == size)
                    return true;
            }
        }
        return true;
    }

    bool operator!=(const CircularBuffer &b ){              //тест есть
        return !(this == &b);
    }

    CircularBuffer & operator=(const CircularBuffer & cb){
        if(this == &cb){
            return *this;
        }
        delete [] arr;
        arr = new NODE [cb.capacity];
        capacity = cb.capacity;
        for(int i = 0 ; i < capacity ; i++){
            if( !cb.arr[i].is_empty){
                arr[i].data = cb.arr[i].data;
                arr[i].is_empty = false;
            }
        }
        capacity = cb.capacity;
        size = cb.size;
        head = cb.head;
    };

//Обменивает содержимое буфера с буфером cb.

    void swap(CircularBuffer & cb){                                 //тест есть
        if(this != &cb){
            std::swap(arr , cb.arr);
            std::swap(size , cb.size);
            std::swap(arr , cb.arr);
            std::swap(head , cb.head);
        }
    };

//Добавляет элемент в конец буфера.

//Если текущий размер буфера равен его ёмкости, то переписывается

//первый элемент буфера (т.е., буфер закольцован).

    void push_back(const value_type & item = value_type()){                     //тест есть
        if( !full()){
            for(int i = 0 ; i < capacity ; i++) {
                if (arr[(head + i) % capacity].is_empty) {
                    arr[size].is_empty = false;
                    arr[size].data = item;
                }
            }
            size++;
        }
        else{
            arr[head].data = item;
            head = (head+1) % capacity;
        }
    };

//Добавляет новый элемент перед первым элементом буфера.

//Аналогично push_back, может переписать последний элемент буфера.

    void push_front(const value_type & item = value_type()){                //тест есть
        if(head > 0)
            head--;
        else
            head = capacity - 1;
        arr[head].data = item;
        arr[head].is_empty = false;
    };

//удаляет последний элемент буфера.

    void pop_back(){                                        //тест есть
        if(!empty()){
            int emptySpace = 0;
            int notEmpty = 0;
            for(int i = 0 ; notEmpty < size && notEmpty < capacity ; i++){
                if(arr[(head + i) % capacity].is_empty)
                    emptySpace++;
                else
                    notEmpty++;
            }
            arr[(head + size-1 + emptySpace) % capacity].is_empty = true;
            arr[(head + size-1 + emptySpace) % capacity].data = 0;
        }
        size--;
    };

//удаляет первый элемент буфера.

    void pop_front() {
        if (!empty()) {
            arr[head].data = 0;
            arr[head].is_empty = true;
            head = (head + 1) % capacity;
        }
    };

//Вставляет элемент item по индексу pos. Ёмкость буфера остается неизменной.

    void insert(int pos, const value_type & item = value_type()){               //тест есть
        if( pos < 0 || pos > capacity) {
            throw (-1);
        }
        if(empty()) {
            rotate(pos);
        }
        arr[pos].data = item;
        if(arr[pos].is_empty) {
            arr[pos].is_empty = false;
            size++;
        }
    };

//Удаляет элементы из буфера в интервале [first, last).

    void erase(int first, int last){                                            //тест есть
        if( first > last || first < 0 || last > capacity){
            throw -1;
        }
        for(int i = first ; i <= last ; i++) {
            if(!arr[i].is_empty) {
                arr[i].data = 0;
                arr[i].is_empty = true;
                size--;
            }
        }
        if( first <= head <= last) {
            if (last == capacity || empty())
                rotate(0);
            else
                rotate(last+1);
        }
    };

//Очищает буфер.

    void clear(){                                                                   //тест есть
        delete [] arr;
        arr = new NODE [capacity];
        size = 0;
        rotate(0);
    };

private:
    NODE *arr;
    int head = 0;
    int capacity = defaultCapcity;
    int size = 0;
};

#endif
