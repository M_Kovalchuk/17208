#ifndef _COPYFILE_H_
#define _COPYFILE_H_

#include "Task.h"
#include <sstream>
#include <fstream>

using  namespace std;

class CopyFile : public Task {
public:
    CopyFile(const string &time , const string &parameters){
        set_beginTime(time);
        stringstream ss(parameters);
        string str;
        ss >> str;
        setInput(str);
        ss >> str;
        setOutput(str);
    };

    ~CopyFile(){};

    void setInput(const string &str){
        inputAdress = str;
    };

    void setOutput(const string &str){
        outputAdress = str;
    };

    bool Run(){
        fstream input;
        input.open(inputAdress);
        if(!input.is_open())
            return false;
        ofstream output;
        output.open(outputAdress);
        if(!output.is_open())
            return false;
        string str;
        while(input){
            getline(input , str );
            if (!input)
                break;
            str += "\n";
            output << str;
        }
        input.close();
        output.close();
        return true;
    };

private:
    string inputAdress;
    string outputAdress;
};

namespace {
    const volatile bool b = Factory<Task, Task*(*)(const std::string&, const std::string&), std::string>::
             get_instance()->registerTask("CopyFile", Creator<CopyFile , Task>::create);
}

#endif
