#ifndef FACTORY_H
#define FACTORY_H

#include <unordered_map>
#include <iostream>

template<class Divider, class Parent>
class Creator{
public:
    static Parent *create( const std::string &time , const std::string &parameters){
        return (new Divider(time , parameters));
    }
};

template<class Product,
         class Creator,
         class ID>
class Factory {
public:
    Factory(){};

    static Factory* get_instance() {
        static Factory f;
        return &f;
    }

    bool registerTask(const ID& id, Creator creator) {
        creators.insert({id, creator});
        return isRegistered(id);
    }

    bool isRegistered(const ID& id){
        return !(creators.find(id) == creators.end());
    }

    Product* create(const ID &id , const std::string &time ,  const std::string &parameters) {
        return creators[id](time , parameters);
    }

    ~Factory(){};
private:
    std::unordered_map<ID, Creator> creators;
};

#endif

//namespace {
//    bool b = Factory<Task, Task*(*)(), std::string>::
//             get_instance()->registerTask("taskName", Creator<taskName , Task>::create);
//}
