#ifndef _SCHEDULE_H
#define _SCHEDULE_H

#include <thread>
#include "Task.h"

using namespace std;

class Schedule {
public:
    Schedule(){};

    Schedule(vector<Task*> &newTasks): tasks(newTasks) {};

    ~Schedule(){};

    bool Run(){
        if(makeThreads()) {
            for (int i = 0; i < threads.size(); i++) {
                threads[i].join();
            }
        }
        else
            return false;
        return true;
    };

    bool makeThreads(){
        if(tasks.size() == 0) {
            return false;
        }
        threads.clear();
        for(int i = 0 ; i < tasks.size() ; i++){
            threads.push_back( thread(&Schedule::thrRun , this,   tasks[i]));
        }
        return true;
    }

    void thrRun(Task* task){
        time_t t = time(0);
        tm now = *localtime(&t);
        while( now != task->starts()){
            t = time(0);
            now = *localtime(&t);
        }
        task -> Run();
    }

    int lastTaskStart(){
        tm l = tasks[0]->starts();
        int n = 0;
        for(int i = 1 ; i < tasks.size() ; i++){
            if( tasks[i]->starts() > l) {
                l = tasks[i]->starts();
                n = i;
            }
        }
        return n;
    };

private:
    vector<thread> threads;
    vector<Task*> tasks;
};

#endif
