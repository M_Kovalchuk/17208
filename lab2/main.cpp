#include <memory>
#include "Parser.h"
#include "Schedule.h"

int main( int argc , char * argv []) {
    std::vector<Task*> tasks = Parser(argv[1]).get_tasks();
    std::cout << "колличество задач:   "  << tasks.size() << std::endl;
    std::unique_ptr<Schedule> schedule = (std::unique_ptr<Schedule>) new Schedule(tasks);
    schedule -> Run();
    return 0;
}
