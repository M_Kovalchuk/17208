#ifndef _TASK_
#define _TASK_

#include <iostream>
#include <string>
#include <chrono>
#include <time.h>
#include "Factory.h"

using namespace std;

class Task{
protected:
    string type;
    tm beginTime;

public:

    Task(){};

    Task(string &newType , string newTime){
        type = newType;
        set_beginTime(newTime);
    }

    ~Task(){};

    void set_beginTime(const string &newTime){
        time_t t = time(0);
        beginTime = *localtime(&t);
        beginTime.tm_hour = (newTime[0] - 0x30) * 10 + (newTime[1] - 0x30);
        beginTime.tm_min = (newTime[3] - 0x30) * 10 + (newTime[4] - 0x30);
        if( newTime.length() > 5 )
            beginTime.tm_sec = (newTime[6] - 0x30) * 10 + (newTime[7] - 0x30);
    }

    void print(){
        cout << "type  " << type << endl;
        printf("start  %s" , asctime(&beginTime));
        cout << endl;
    }

    virtual bool Run(){};

    tm starts(){
        return beginTime;
    }

};

//class taskName : public Task{
//
//};

//namespace {
//    bool b = Factory<Task, Task*(*)(), std::string>::
//    get_instance()->registerTask("taskName", Creator<taskName , Task>::create);
//}

bool operator ==(const tm &a ,const tm &b){
    if( a.tm_year == b.tm_year &&
        a.tm_mon == b.tm_mon &&
        a.tm_mday == b.tm_mday &&
        a.tm_hour == b.tm_hour &&
        a.tm_min == b.tm_min &&
        a.tm_sec == b.tm_sec)
        return true;
    return false;
}

bool operator !=(const tm &a ,const tm &b){
    return !(a == b);
}

bool operator >=(const tm &a , const tm &b){
    if( a == b )
        return true;
    if( a.tm_year > b.tm_year)
        return true;
    if( a.tm_mon > b.tm_mon)
        return true;
    if( a.tm_mday > b.tm_mday)
        return true;
    if( a.tm_hour > b.tm_hour)
        return true;
    if( a.tm_min > b.tm_min)
        return true;
    if( a.tm_sec > b.tm_sec)
        return true;
    return false;
}

bool operator <=(const tm &a , const tm &b){
    if( a == b )
        return true;
    if(!(a >= b))
        return true;
    return false;
}

bool operator >(const tm &a , const tm &b){
    if( a >= b && !(a == b))
        return true;
    return false;
}

bool operator <(const tm &a , const tm &b){
    if( a <= b && !(a == b))
        return true;
    return false;
}

#endif
