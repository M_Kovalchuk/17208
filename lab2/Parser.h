#ifndef _PARSER_
#define _PARSER_

#include <vector>
#include "CopyFile.h"

using namespace std;

class Parser {
    public:
        Parser(){};

        Parser(char* &path ){
            read_tasks(path);
        }

        ~Parser(){};

        void read_tasks(char* &openPath){
            file.open(openPath);
            if(!is_open()){
                throw 1;
            }
            string str;
            string newTime;
            string type;
            while(file){
                file >> newTime >> type;
                cout << newTime << "  ";
                cout << type << endl;
                getline(file , str );
                if (!file)
                    break;
                if( validateTime(newTime) && validateType(type)) {
                    tasks.push_back(Factory<Task, Task*(*)(const std::string&, const std::string&), std::string>::
                            get_instance()->create(type , newTime , str));
                }
            }
            file.close();
        };

        bool validateTime(string &newTime){
            if( newTime.length() != 5 && newTime.length() != 8 )
                return false;
            for(int i = 0 ; i < newTime.length() - 1 ; i += 3){
                if( isdigit(newTime[i]) && isdigit(newTime[i+1]) && !isdigit(newTime[i+2]))
                    if( i == 0 && ((newTime[i] - 0x30) * 10 + (newTime[i] - 0x30)) > 23 )
                        return  false;
                    if( (newTime[i] - 0x30) > 6 )
                        return false;
            }
            time_t t = time(0);
            tm now = *localtime(&t);
            if( now.tm_hour > ((newTime[0] - 0x30) * 10 + (newTime[1] - 0x30)))
                return false;
            if( now.tm_hour == ((newTime[0] - 0x30) * 10 + (newTime[1] - 0x30)) &&
                now.tm_min > ((newTime[3] - 0x30) * 10 + (newTime[4] - 0x30)))
                return false;
            if( newTime.length() == 8 )
                if( now.tm_hour == ((newTime[0] - 0x30) * 10 + (newTime[1] - 0x30)) &&
                    now.tm_min > ((newTime[3] - 0x30) * 10 + (newTime[4] - 0x30)) &&
                    now.tm_sec > ((newTime[6] - 0x30) * 10 + (newTime[7] - 0x30)))
                    return false;
            return true;
        };

        bool validateType(string &type){
            return Factory<Task, Task*(*)(const std::string&, const std::string&), std::string>::get_instance()->isRegistered(type);
        };

        bool is_open(){
            return file.is_open();
        };

        vector<Task*> get_tasks(){
            return tasks;
        }

    private:
        ifstream file;
        vector<Task*> tasks;
};

#endif
